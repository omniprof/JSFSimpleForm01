This project has been updated to use MySQL 8. Before running you should execute the SQL scripts in src/test/resources. 

In glassfish-resources.xml for MySQL 5 it reads:
<pre><code>
&lt;property name="URL" value="jdbc:mysql://localhost:3306/JSFEXERCISE?zeroDateTimeBehavior=convertToNull"/&gt;
</code></pre>
The datasource must also be changed. Here is the original: 
<pre><code>
&lt;jdbc-connection-pool ... datasource-classname="com.mysql.jdbc.jdbc2.optional.MysqlDataSource" ... &gt;
</code></pre>

while for MySQL 8 it must read

<pre><code>
&lt;property name="URL" value="jdbc:mysql://localhost:3306/JSFEXERCISE?zeroDateTimeBehavior=CONVERT_TO_NULL&amp;useSSL=false"/&gt;
</code></pre>

Here is the new MySQL 8 datasource

<pre><code>
&lt;jdbc-connection-pool ... datasource-classname="com.mysql.cj.jdbc.MysqlDataSource" ... &gt;
</code></pre>

The server is Payara 5.
Using Java 1.8.
IDE is NetBeans 8.2 but as a Maven project you should be able to use any any IDE.
