package com.kfwebstandard.jsfsimpleform01.controller;

import com.kfwebstandard.jsfsimpleform01.entities.Person;
import com.kfwebstandard.jsfsimpleform01.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
@Named
@RequestScoped
public class PersonJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PersonJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "SimpleFormPU")
    private EntityManager em;

    public void create(Person person) throws RollbackFailureException {
        try {
            utx.begin();
            em.persist(person);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

}
